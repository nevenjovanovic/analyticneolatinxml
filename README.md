# An XML version of the Analytic Neo-Latin Bibliography #

[Dana F. Sutton's excellent collection](http://www.philological.bham.ac.uk/bibliography/) is hand-tagged and hard to search. An XML version would be easier to validate, navigate, and correct. Here we experiment with how to create and update it.

### Directories ###

* bibliography: HTML files harvested from the site
* bibliogtxt: text files, produced with html2text
* bibliogxml: XML files produced from text files, with very simple tagset for fields and url
* root: scripts, bash and otherwise

### How do I get set up? ###

* Experimental database deployment at the [test server](http://solr.ffzg.hr/basex/rest/anlb)
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact