#!/bin/bash
# Jovanovic, 2014-08, convert multiple txt to xml using sed
# usage: ./neolattxt2xml.sh

for i in *.txt; do
  sed -e 's%^A_-_Al | Am_.*%%g; s%^ *\(AUTHOR.*\)%<rc><a>\1</a>%g; s%^ *\(TITLE\)\(.*\)%<t>\1 \2</t>%g; s%^ *\(SUBJECT.*\)%<s>\1</s>%g; s%^ *\(NOTES.*\)%<n>\1</n></rc>%g; s%^ *\(SITE.*\)%</u><st>\1</st>%g; s%^ *\(URL.*\)%<u>\1%g; s%&O%\&amp;O%g' ${i} > ${i}.xml
done
