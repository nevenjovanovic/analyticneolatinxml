#!/bin/bash
# Jovanovic, 2014-08, convert multiple html to txt using html2text
# usage: ./neolat2txt.sh

for i in h*.html; do
  /usr/bin/html2text -o /home/neven/rad/neolat/neolatbib/${i}.txt -width 10000 ${i}
done
