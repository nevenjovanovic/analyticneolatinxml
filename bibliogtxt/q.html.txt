
A_-_Al | Am_-_Az | Ba | Be_-_Bi | Bl_-_Bo | Br_-_Bz | Ca_-_Ce | Ch_-_Cz | D | E | F | G_-_Gi |Gl_-_Gy |_Ha_-_He | Hi_-_Hy | I | J | K | L_-_Lh | Li_-_Ly | Ma | Me | Mi_-_My | N | O | Pa_-_Pi | Pl_-_Py | Q | R | Sa_-_Se | Sf_-_Sz | T | U | V | W | Y | X | Z | Anon._A_-_D | Anon._E_-_P | Anon._Q_-_Z | Welcome_Page
 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       Q
 
AUTHOR Quad, Matthias
TITLE Deliciae Germaniae sive totius Germaniae itinerarium
URL https://play.google.com/books/reader?id=WBM8AAAAcAAJ&amp;printsec=frontcover&amp;output=reader&amp;authuser=0&amp;hl=en
SITE Google Books
SUBJECT Topography
NOTES Dpr of the 1600 Florence edition
AUTHOR Quad, Matthias
TITLE Europae totius terrarum orbis partis praestantissimae, generalis ac particularis descriptio
URL https://play.google.com/books/reader?id=2Xzye06H6IoC&amp;printsec=frontcover&amp;output=reader&amp;authuser=0&amp;hl=en
SITE Google Books
SUBJECT Geography
NOTES Dpr of the 1596 Cologne edition
AUTHOR Quad, Matthias
TITLE Memorabilia mundi
URL http://daten.digitale-sammlungen.de/~db/0003/bsb00033595/images/
SITE MDZ
SUBJECT Wonders (?)
NOTES Dpr of the 1601 Constanz edition
AUTHOR Quadrantinus, Fabianus
TITLE Palinodiae Sive Revocationes Fabiani Qvadrantini, Cvm factus esset ex Lutherano Catholicus recitatae Braunsbergae in Collegio Societatis Iesu
URL http://daten.digitale-sammlungen.de/~db/0002/bsb00027973/images/
SITE MDZ
SUBJECT Religion
NOTES Dpr of the 1571 Cologne edition
AUTHOR Quadratus, Mathurius
TITLE In Ioelem prophetam homeliae
URL http://lubna.uv.es:83/Z_9_189/Z_9_189_fich_1.html
SITE Universitat de València Biblioteca Digital
SUBJECT Religion
NOTES Dpr of the 1582 Paris edition
AUTHOR Quadros, Diego de
TITLE Palaestra biblica, sive Enchiridion neotericorum pro sacris codicibus rite tractandis
URL http://bibliotecaforal.bizkaia.net/search/te/te/48,234,297,B/l962&amp;FF=tenchiridion+neotericorum+pro+sacris+codicibus+rite+tractandis+et+dificultatibus+sacrae+scripturae+scholastice+discutiendis+to&amp;1,1,,003058,-1
SITE Bizkaiko Foru Aldunia
SUBJECT Religion
NOTES Dpr of the 1727 Madrid edition
AUTHOR Quainus, Hieronymus
TITLE Oratio gratulatoria in adventu reverendiss. d. domini Nicolai Ormanetti episcopi Patavini
URL http://daten.digitale-sammlungen.de/~db/0003/bsb00030451/images/
SITE MDZ
SUBJECT Rhetoric
NOTES Dpr of the 1572 Padua edition
AUTHOR Qualm, Bartholomaeus
TITLE Columba Continens elegantem allegoriam vitae coniugalis
URL http://daten.digitale-sammlungen.de/~db/0003/bsb00039230/images/
SITE MDZ
SUBJECT Poetry
NOTES Dpr of the 1571 Görlitz edition
AUTHOR Quandt, Johann Jakob
TITLE Gesta Christi Quadragesimalia Resurrectionem Inter Et Ascensionem A Whistoni, Flaminii, Dodwelli, Harduini, Aliorumque Paradoxis Liberata
URL http://digitale.bibliothek.uni-halle.de/urn/urn:nbn:de:gbv:3:1-120571
SITE ULB Sachsen-Anhalt
SUBJECT Religion
NOTES Dpr of the 1716 Königsberg edition
AUTHOR Quatriganus, Claudius (Claude Cassegrain)
TITLE Versus de Tempensium iure municipali conscripto
URL http://www.corpusetampois.com/cls-16-quatigranus1556versus.html
SITE Corpus Latinum Stampense
CONTRIBUTOR Bernard Gineste
SUBJECT Poetry
NOTES Dpr of the 1556 edition with html transcript
AUTHOR Queccius, Gregor
TITLEAnatomia Philologica. Continens Discursus Philologicos De Nobilitate Et Praestantia Hominis Contra iniquos conditionis humanae aestimatores
URL http://nbn-resolving.de/urn:nbn:de:bvb:12-bsb00001248-4
SITE MDZ
SUBJECT Philology (?)
NOTES Dpr of the 1632 Nuremberg edition
AUTHOR Quedlinburg, Jordan von O. E. S. A. (d. 1380)
TITLE Sermones et postillae de tempore
URL http://fondosdigitales.us.es/books/digitalbook_view?oid_page=113200
SITE Fondos Digitalizados de la Universidad de Sevilla
SUBJECT Religion
NOTES Dpr of the 1483 Strassburg edition
AUTHOR Quell, Christian Friedrich
TITLE Prolvsio De Persecvtionibvs Vniversalibvs
URL http://digitale.bibliothek.uni-halle.de/urn/urn:nbn:de:gbv:3:1-134186
SITE ULB Sachsen-Anhalt
SUBJECT Religion
NOTES Dpr of the 1762 Friedrichstadt edition
AUTHOR Quennerstedt, August Wilhelm
TITLE Notes
URL http://waller.ub.uu.se/object.xsql?DBID=17981
SITE Uppsala University Library
SUBJECT Miscellaneous
NOTES Dpr of Waller Manuscript Collection ms. se-03572
AUTHOR Quensel, Conrad
TITLE Album amicorum (1710)
URL http://waller.ub.uu.se/object.xsql?DBID=16395
SITE Uppsala University Library
SUBJECT Miscellaney
NOTES Dpr of Waller Manuscript Collection ms. se-02540
AUTHOR Quensel, Conrad
TITLE Dissertatio hist.-nat. ignotas insectorum species continens
URL http://resolver.sub.uni-goettingen.de/purl?PPN581187776
SITE GDZ
SUBJECT Zoology
NOTES Dpr of the 1790 Lund edition
AUTHOR Quenstedt, Johann Andreas (1617 - 1688)
TITLE Disputatio politica de maiestate eiusdemque iuribus
URL http://nbn-resolving.de/urn:nbn:de:bvb:12-bsb00001766-9
SITE Münchener Digitalisierungszentrum
SUBJECT Politics
NOTES Dpr of the 1657 Wittemberg edition
AUTHORS Quenstedt, Johann Andreas (1617 - 1688) et al.
TITLE Protelia Votiva
URL http://digitale.bibliothek.uni-halle.de/content/titleinfo/844387
SITE ULB Sachsen-Anhalt
SUBJECT Poetry
NOTES Dpr of the 1661 Wittemberg edition
AUTHOR Quenstedt, Johann Andreas (1617 - 1688)
TITLE Rector Academiae Wittenbergensis, Johannnes Andreas Quenstedt
URL http://www.gbv.de/du/services/gLink/vd17/547:628357P_001,800,600
SITE VD17
SUBJECT Education
NOTES Dpr of the 1680 edition
AUTHOR Quenstedt, Johann Andreas (1617 - 1688)
TITLE Rector Academiae Wittenbergensis, Johannnes Andreas Quenstedt
URL http://www.gbv.de/du/services/gLink/vd17/547:629976F_001,800,600
SITE VD17
SUBJECT Education
NOTES Dpr of the 1680 edition
AUTHOR Quenstedt, Johann Andreas (1617 - 1688)
TITLE Rector Academiae Wittenbergensis, Johannnes Andreas Quenstedt
URL http://www.gbv.de/du/services/gLink/vd17/125:046759L_001,800,600
SITE VD17
SUBJECT Education
NOTES Dpr of the 1681 edition
AUTHOR Quenstedt, Johann Andreas (1617 - 1688)
TITLE Rector Academiae Wittenbergensis, Johannnes Andreas Quenstedt
URL http://www.gbv.de/du/services/gLink/vd17/547:628699W_001,800,600
SITE VD17
SUBJECT Education
NOTES Dpr of the 1681 edition
AUTHOR Quenstedt, Johann Andreas (1617 - 1688)
TITLE Rector Academiae Wittenbergensis, Johannnes Andreas Quenstedt
URL http://www.gbv.de/du/services/gLink/vd17/547:628942B_001,800,600
SITE VD17
SUBJECT Education
NOTES Dpr of the 1681 edition
AUTHOR Quenstedt, Johann Andreas (1617 - 1688)
TITLE Rector Academiae Wittenbergensis, Joh. Andreas Quenstedt
URL http://www.gbv.de/du/services/gLink/vd17/547:629848W_001,800,600
SITE VD17
SUBJECT Education
NOTES Dpr of the 1681 edition
AUTHOR Quenstedt, Johann Andreas (1617 - 1688)
TITLE Rector Academiae Wittenbergensis, Joh. Andreas Quenstedt
URL http://www.gbv.de/du/services/gLink/vd17/1:086814Q_001,800,600 through
http://www.gbv.de/du/services/gLink/vd17/1:086814Q_004,800,600
SITE VD17
SUBJECT Education
NOTES Dpr of the 1687 edition
AUTHOR Quenstedt, Johann Andreas (1617 - 1688)
TITLE Rector Academiae Wittenbergensis, Joh. Andreas Quenstedt
URL http://www.gbv.de/du/services/gLink/vd17/3:625411K_001,800,600
SITE VD17
SUBJECT Education
NOTES Dpr of the 1687 edition
AUTHORS Quenstedt, Johann Andreas (1617 - 1688) et al.
TITLE Summi in Theolgia Honoris Templum Paulo Philippo Roebero, SS. Theol. Lic. Ecclesiae Freibergensis Pastori Vicinarumque Superintendenti Wittebergae d. 20. April. MDCLXXV. ingresso applaudebant Patroni Et Fautores
URL http://digitale.bibliothek.uni-halle.de/content/titleinfo/857127
SITE ULB Sachsen-Anthalt
SUBJECT Poetry
NOTES Dpr of the 1675 Wittemberg edition
AUTHOR Quentel, Johann
TITLE Interim, hoc est, constitutio, praescribens qua ratione sacrosancti imperii Romani status in negocio religionis usque ad decisionem concilii Tridentini sese mutuo gererere ac excipere
URL http://digitale.bibliothek.uni-halle.de/urn/urn:nbn:de:gbv:3:1-114232
SITE ULB Sachsen-Anhalt
SUBJECT Religion
NOTES Dpr of the 1548 Cologne edition
AUTHOR Quentin, Jean (d. 1503)
TITLESermones aurei super evangelia dominicarum totius anni
URL http://visualiseur.bnf.fr/Visualiseur?Destination=Gallica&O=NUMM-053879
SITE Gallica - Bibliothèque nationale de France
SUBJECT Religion
NOTES Dpr of the 1476 edition; downloadable pdf and tiff formats
AUTHOR Quentin, Jean (d. 1503)
TITLESermones dominicales moralissimi et ad populum instruendum exquisitissimi
URL http://visualiseur.bnf.fr/Visualiseur?Destination=Gallica&O=NUMM-054321
SITE Gallica - Bibliothèque nationale de France
SUBJECT Religion
NOTES Dpr of the 1480 edition; downloadable pdf and tiff formats
AUTHOR Quentin, Jean (d. 1503) (pretending to be St. Bonaventure)
TITLE Stimulus divini amoris
URL http://fondosdigitales.us.es/books/digitalbook_view?oid_page=132633
SITE Fondos Digitalizados de la Universidad de Sevilla
SUBJECT Religion
NOTES Dpr of an undated Paris edition
AUTHOR Querenghi, Antonio
TITLE Hexametri Carminis Libri Sex
URL http://daten.digitale-sammlungen.de/~db/0004/bsb00043287/images/
SITE MDZ
SUBJECT Poetry
NOTES Dpr of the 1618 Rome edition
AUTHOR Querini, Angelo Maria Cardinal
TITLE De optimorum scriptorum editionibus quae Romae primum prodierunut post divinum typographiae inventum
URLhttps://play.google.com/books/reader?id=-zxRAAAAcAAJ&amp;printsec=frontcover&amp;output=reader&amp;authuser=0&amp;hl=en&amp;pg=GBS.PP5
SITE Google Books
SUBJECT Library science
NOTES Dpr of the 1761 Lindau edition
AUTHOR Querini, Angelo Maria Cardinal
TITLEEminentissimus, ac Reverendissimus Dominus Dominus Angelus Maria ... Cardinalis ... Ex Antiquissima ... Familia De Quirinis, Sacrae Romanae Ecclesiae Bibliothecarius, Ac Episcopus Brixiensis Ab Abrahamo Tegernseensi, Et eiusdem Monasterii Musis humillimum in obsequium festive exceptus Die XXV. Septembris, Anno M.DCC.XLVIII
URL http://epub.ub.uni-muenchen.de/2743/
SITE Ludwig-Maximilians-Universität Munich
SUBJECT Religion
NOTES Dpr of the 1748 Tegernsee edition
AUTHOR Quevedo, Bartolomé de
TITLECommentary onDe vita et honestate clericorumof Pope John XXII
URLhttp://www.chmtl.indiana.edu/tml/16th/QUECOM_TITLE.html
SITE Thesaurus Musicarum Latinarum
SUBJECT Music
NOTES Html format
AUTHOR Quevedo, Franciso de (1580 - 1645)
TITLE Roma
URL http://digilander.iol.it/Marziale/Grex/exempla/caro.html
SITE Grex Alter Latine Loquentium
CONTRIBUTOR Joseph Sanctius
SUBJECT Poetry
NOTES Translated by Michael Antonius Caro (1843 - 1909); html format
AUTHOR Queyrats, Louis
TITLE Tractatus de vulneribus capitis
URL http://alfama.sim.ucm.es/dioscorides/consulta_libro.asp?ref=X533888327
SITE Biblioteca Universidad Complutense
SUBJECT Medicine
NOTES Dpr of the 1657 Tolouse edition
AUTHOR Quichelberg, Samuel
TITLE Inscriptiones Vel Titvli Theatri Amplissimi Complectentis rerum vniuersitatis singulas materias et imagines eximias, ut idem recte quoq[ue] dici possit: Promptuarium artificiosarum miraculasarumq[ue] ac omnis rari thesauri et pretiosae suppellectilis
URL http://daten.digitale-sammlungen.de/~db/0002/bsb00025047/images/
SITE MDZ
SUBJECT Encyclopedic learning
NOTES Dpr of the 1565 Munich edition
AUTHOR Quintianus, Vincentius
TITLE Contra diversas haereses nuper collecta fragmenta
URL http://daten.digitale-sammlungen.de/~db/0002/bsb00024582/images/
SITE MDZ
SUBJECT Religion
NOTES Dpr of the 1557 Mantua edition
AUTHOR Quintilian: see Heinrich_Babucke
AUTHOR Quintilian: see Josse_Bade (two items)
AUTHOR Quintilian: seeCristoforo_Barzizza
AUTHOR Quintilian: see Domizio_Calderini
AUTHOR Quintilian: see Giannantonio_Campano (two items)
AUTHOR Quintilian: see Carolus_Georgius_Ekmark (two items)
AUTHOR Quintilian: seeJacobus_Grasolarius
AUTHOR Quintilian: see Philip_Melanchthon
AUTHOR Quintilian: see Giorgio_Merula
AUTHOR Quintilian: see Friedrich_Osann
AUTHOR Quintilian: see Angelus_Politianus
AUTHOR Quintilian: see Raphael_Regius (two items)
AUTHOR Quintilian: see Cipriano_Suárez
AUTHOR Quintilian: see Thadaeus_Ugoletus
AUTHOR Quintilian: see Lorenzo_Valla
AUTHOR Quintilian: see Philologica:_De_Stvdio,_Stylo_et_Et_Artificio_Epistolico:_F._Qvintiliani;_E._Roterodami;_A._Senecae;_Plinii;_D._Phalerei;_G._Nazianzeni_&amp;_Libanij,_Sapientissimorvm_Virorum_Placita
AUTHOR Quintin, Jean (1500-1561)
TITLEInsulae Melitae descriptio
URL http://visualiseur.bnf.fr/Visualiseur?Destination=Gallica&O=NUMM-079280
SITE Gallica - Bibliothèque nationale de France
SUBJECT Topography
NOTES Dpr of the 1536 Lyon edition; downloadable pdf and tiff formats
AUTHOR Quintus Septimius Florens Rivinus (Johann Gottfried Böhme)
TITLE Schediasma De noctu lucentibus
URL http://digital.slub-dresden.de/ppn273291696
SITE Sächsische Landesbibliothek Staats- und Universitätsbibliothek Dresden
SUBJECT Meteorology
NOTES Dpr of the 1673 Leipzig edition
AUTHOR Quintus Smyrnaeus: see Jean_Brodeau
AUTHOR Quintus Smyrnaeus: see Johann_Freige
AUTHOR Quiroga, Gaspar de
TITLE [Carta del cardenal Quiroga a Gregorio XIII, la cual envó junto con el libro tercero de concilios]
URL
http://saavedrafajardo.um.es/biblioteca/biblio.nsf/novedades/871D5EA37A2BD681C1256E8D00379B85
SITE Biblioteca Virtual Saavedra Fajardo
SUBJECT Religion
NOTES Dpr of the 1803 Madrid edition
AUTHOR Quiroga, Gaspar de
TITLE [Carta del obispo de Cuenca e inquisidor general D. Gaspar De Quiroga, al papa Gregorio XIII, enviándole el libro primero de los concilios de España]
URL http://saavedrafajardo.um.es/biblioteca/biblio.nsf/novedades/42E0CA25E51309EAC1256E8D00379119
SITE Biblioteca Virtual Saavedra Fajardo
SUBJECT Religion
NOTES Dpr of the 1803 Madrid edition
 
A_-_Al | Am_-_Az | Ba | Be_-_Bi | Bl_-_Bo | Br_-_Bz | Ca_-_Ce | Ch_-_Cz | D | E | F | G_-_Gi |Gl_-_Gy |_Ha_-_He | Hi_-_Hy | I | J | K | L_-_Lh | Li_-_Ly | Ma | Me | Mi_-_My | N | O | Pa_-_Pi | Pl_-_Py | Q | R | Sa_-_Se | Sf_-_Sz | T | U | V | W | Y | X | Z | Anon._A_-_D | Anon._E_-_P | Anon._Q_-_Z | Welcome_Page
